function page_loaded() {
    let numberOfFilms
    do {
        numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?', '')
    } while (numberOfFilms === null || numberOfFilms.length === 0 || isNaN(+numberOfFilms))

    const personalMovieDB = {
        count: numberOfFilms,
        movies: {},
        actors: {},
        genres: [],
        privat: false,
        showMyDB: function () {
            if (!personalMovieDB.privat) {
                console.log(personalMovieDB)
            }
        },
        writeYourGenres: function () {
            for (let i = 0; i < 3; i++) {
                let genre
                do {
                    genre = prompt(`Ваш любимый жанр под номером ${i + 1}`, '')
                } while (genre === null || genre.length === 0)
                personalMovieDB.genres.push(genre)
            }
            personalMovieDB.genres.forEach((elem, index) => console.log(`Любимый жанр #${index + 1} - это ${elem}`))
        },
        addMovies: function () {
            let movie, rating
            for (let i = 0; i < personalMovieDB.count; i++) {
                do {
                    movie = prompt('Один из последних просмотренных фильмов?', '')
                } while (movie === null || movie.length === 0 || movie.length > 50)

                do {
                    rating = prompt('На сколько оцените его?', '')
                } while (rating === null || rating.length === 0)

                personalMovieDB.movies[movie] = rating
            }
        },
        rateMyself: function () {
            if (personalMovieDB.count < 10) console.log('Просмотрено довольно мало фильмов')
            else if (personalMovieDB.count >= 10 && personalMovieDB.count <= 30) console.log('Вы классический зритель')
            else if (personalMovieDB.count > 30) console.log('Вы киноман')
            else console.log('Произошла ошибка')
        },
        toggleVisibleMyDB: function () {
            personalMovieDB.privat = !personalMovieDB.privat
        }
    }
    // personalMovieDB.addMovies()
    // personalMovieDB.writeYourGenres()
    // personalMovieDB.rateMyself()
    // personalMovieDB.toggleVisibleMyDB()
    // personalMovieDB.showMyDB()
    // personalMovieDB.toggleVisibleMyDB()
    // personalMovieDB.showMyDB()
    // можно раскомментировать и проверить успешность выполнения
}

document.addEventListener('DOMContentLoaded', page_loaded)
